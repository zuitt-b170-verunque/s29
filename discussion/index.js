// loads/imports the express package
let express = require("express");

// creates an application using - express()
const app = express();

const port = 3000;


let database = [
    {
        "name" : "Brandon",
        "password" : "password"
    },
    {
        "name" : "Jobert",
        "password" : "password2"
    }
]



// use function lets the middleware to do common services and capabilities to the applications
app.use(express.json()); //lets the app to read json data
app.use(express.urlencoded({extended:true})); //extended:true, allows the app to receive data from the forms
// this two code is necessary so that our req.body will not return undefined


// app (the variable name with the express package)."get" or any method i.e "POST", "PATCH" ([url], (req, res))
app.get("/", (req, res)=>{
    res.send("Hello World")
})

// MINI Activity
app.get("/hello", (req, res)=>{
    res.send("Hello from /hello endpoint")
})

app.post("/hello", (req, res)=>{
    console.log(req.body.firstName);
    console.log(req.body.lastName);
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


// ACTIVITY

// home
app.get("/home", (req, res)=>{
    res.send("Welcome to Home Page")
})

// Users
app.get("/users", (req, res)=>{
    res.send(database)
})

// Deletion
app.delete("/delete-users", (req, res)=>{
    res.send(`${req.body.name} has been deleted`)
})


// STRETCH GOAL
/* 
-create a users variable that will accept an array of objects
-create /signup route that will accept post method.
-once the username and the password in the body is filled with information, push the object into the users array and print the message of "username has registered successfully", otherwise, send "please input both username and password"
*/
app.post("/signup", (req, res)=>{
    let newUser = {
        "name" : req.body.name,
        "email" : req.body.password
    }
    database.push(newUser);
    res.send(`user ${req.body.name} successfully registered`)
    console.log(database)
})

app.listen(port, ()=> console.log(`Server is running at port ${port}`));










